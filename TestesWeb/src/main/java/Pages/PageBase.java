package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PageBase {
	WebDriver navegador;
	public PageBase() {
		System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe");
		navegador = new ChromeDriver();
		setURL("https://www.phptravels.net/admin-portal/admin");
	}
	
	
	public void setURL(String URL) {
		navegador.get(URL);
	}
	
	public void exitChromedrive() {
		navegador.quit();
	}
	
}
