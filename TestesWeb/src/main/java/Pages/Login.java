package Pages;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Login extends PageBase {
	public void fillEmailText() throws InterruptedException {
		WebElement emailText = navegador.findElement(By.xpath("//input[@name='email']"));
		WebElement passwordText = navegador.findElement(By.xpath("//input[@name='password']"));
		WebElement buttonSubmit = navegador.findElement(By.xpath("//button[@type='submit']"));
		emailText.sendKeys("admin@phptravels.com");
		passwordText.sendKeys("demoadmin");
		buttonSubmit.click();
	}
	
	public void openSupplierAccont() throws InterruptedException {
		WebElement accountLink = navegador.findElement(By.xpath("//a[@href='#ACCOUNTS']"));
		accountLink.click();
	}
}
