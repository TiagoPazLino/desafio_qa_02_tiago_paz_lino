#language:pt

Funcionalidade: Cadastro de um Supplier
	Como admin entrar com os dados necessários
	Para cadastro de um Supplier
	

Cenario:Cadastrar
	Dado que eu esteja logado
	Quando eu preencher os dados de SUPPLIER
	E clique em cadastrar
	Então o SUPPLIER deve ser cadastrado com sucesso