package tarefa.TestesWeb;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Quando;
import cucumber.api.java.pt.Entao;
import Pages.Login;

public class BDDPassosSite {
	Login login = new Login();
	
	@Dado("que eu esteja logado")
	public void Login() throws InterruptedException {
		login.fillEmailText();
	}
	
	@Quando("eu preencher os dados de SUPPLIER")
	public void FillSUPPLIERData() throws InterruptedException {
		login.openSupplierAccont();
	}
	
	@Quando("clique em cadastrar")
	public void RegisterSUPPLIER() {
		
	}
	
	@Entao("o SUPPLIER deve ser cadastrado com sucesso")
	public void SUPPLIERShouldBeRegistered() {
		
	}
}
