package tarefa.TestesWeb;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Quando;
import cucumber.api.java.pt.Entao;

public class BDDPassosServiceGET {
	@Quando("eu solicitar dados do site")
	public void requestSiteData() {
		
	}
	
	@Entao("eu devo receber os dados com sucesso")
	public void shouldReceiveData() {
		
	}
	
	@Entao("exibir apenas os itens que estão marcados como completos")
	public void shouldShowCompletedEntries() {
		
	}
}
